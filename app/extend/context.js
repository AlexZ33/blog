'use strict';

module.exports = {

  /**
   *  扩展ctx.success()方法
   *  @param {string} [content] 需要返回给前端的alert内容
   *  @param {string} [handle] 后续操作
   */
  success(content = '', handle = '') {
    this.status = 200;
    this.body = `<script>alert('${content}');${handle}</script>`;
  },

  /**
   *  扩展ctx.failed()方法
   *  @param {string} content 需要返回给前端的alert内容
   *  @param {string} [handle] 后续操作，默认go(-1)
   */
  failed(content, handle = 'history.go(-1);') {
    this.status = 200;
    this.body = `<script>alert('${content}');${handle}</script>`;
  },

  /**
   *  扩展ctx.ajaxSuccess()方法
   *  @param {*} [message] 响应内容
   *  @param {number} [code=200] 响应HTTP状态码
   */
  ajaxSuccess(message, code = 200) {
    this.status = code;
    this.body = {
      success: true,
      message,
    };
  },

  /**
   *  扩展ctx.ajaxFailed()方法
   *  @param {*} [message] 响应内容
   *  @param {number} [code=200] 响应HTTP状态码
   */
  ajaxFailed(message, code = 200) {
    this.status = 200;
    this.body = {
      success: false,
      message,
    };
  },
};
