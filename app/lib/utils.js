'use strict';

const path = require('path');
const { promises: fs } = require('fs');

module.exports = class Utils {
  constructor(app) {
    this.app = app;
    this.config = app.config;
    this.logger = app.logger;
  }

  /**
   * 拷贝文件
   * @async
   * @param {*} file 需要上传的file
   * @return {Promise<string>} 返回新的路径
   */
  async uploadFile(file) {
    const tmpPath = file.filepath;
    const ext = path.extname(file.filename);
    const newName = '' + (new Date().getTime()) + Math.round(Math.random() * 10000) + ext;
    const newPath = '/public/upload/' + newName;
    try {
      // 进行文件拷贝
      const data = await fs.readFile(tmpPath);
      await fs.writeFile(__dirname + '/..' + newPath, data);
      return newPath;
    } catch (error) {
      this.logger.error(new Error(error));
    } finally {
      // 删除临时文件
      await fs.unlink(tmpPath);
    }
  }

  /**
   * 分页函数
   * @param {number} total 总博客数
   * @param {number} page 当前页码
   * @param {number} size 每页数量
   * @return {{limit:number,offset:number,show:string}} 返回数据
   */
  pages(total, page = 1, size = 6) {
    const limit = (page - 1) * size;
    const offset = size;
    const pages = Math.ceil(total / size);
    let show = '';
    page = +page;
    show += '<a href="?page=1" class="page-link">首页</a>';
    show += `<a href="?page=${page - 1 >= 1 ? page - 1 : 1}" class="page-link">上一页</a>`;
    let flag;
    for (let index = 1; index <= pages; index++) {
      page === index ? flag = 'current' : flag = '';
      show += `<a href="?page=${index}" class="${flag}">${index}</a>`;
    }
    show += `<a href="?page=${page + 1 <= pages ? page + 1 : page}" class="page-link">下一页</a>`;
    show += `<a href="?page=${pages}" class="page-link">尾页</a>`;
    return {
      limit,
      offset,
      show,
    };
  }
};

