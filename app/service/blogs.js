'use strict';

const Service = require('egg').Service;
const dayjs = require('dayjs');

class BlogsService extends Service {
  async find(page, search) {
    const { app } = this;
    const size = 6;
    let total = await app.mysql.query(`
    SELECT COUNT(*) AS total
    FROM blogs
    `);
    total = total[0].total;
    const fpage = app.lib.utils.pages(total, page, size);
    const rows = await app.mysql.query(`
    SELECT blogs.*,blogstype.name tname
    FROM blogs,blogstype
    WHERE blogs.title LIKE ? AND
    blogs.cid = blogstype.id
    ORDER BY top DESC, blogs.id DESC
    LIMIT ${fpage.limit},${fpage.offset}`, [ `%${search}%` ]
    );
    rows.forEach(element => {
      element.time = dayjs(element.time * 1000).format('YYYY年MM月DD日 HH:mm:ss');
    });
    return {
      rows,
      show: fpage.show,
    };
  }
  async findTypes() {
    const { app } = this;
    return await app.mysql.select('blogstype', {
      orders: [[ 'id', 'desc' ]],
    });
  }
  async postAdd(params) {
    const { app } = this;
    await app.mysql.insert('blogs', {
      ...params,
      comment: 0,
    });
  }
  async edit(id) {
    const { app } = this;
    const rows = await app.mysql.select('blogs', {
      where:
      {
        id,
      },
    });
    return rows[0];
  }
  async postEdit(id, type, title, keywords, description, img, author, cid, top, content, comment_status) {
    const { app } = this;
    await app.mysql.update('blogs', {
      id,
      type,
      title,
      keywords,
      description,
      img,
      author,
      cid,
      top,
      content,
      comment_status,
    });
  }
  async delete(id) {
    const { app, ctx } = this;
    // 查询是否有对应数据
    const findOne = await app.mysql.get('blogs', {
      id,
    });
    if (!findOne) {
      ctx.logger.error(new Error('不存在该博客id！'));
      throw new Error('不存在该分类id！');
    }
    await app.mysql.beginTransactionScope(async conn => {
      await conn.delete('blogs', {
        id,
      });
      await conn.delete('comment', {
        blog_id: id,
      });
      await conn.delete('reply', {
        blog_id: id,
      });
      return { success: true };
    }, ctx);
  }
}

module.exports = BlogsService;
