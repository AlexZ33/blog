'use strict';

const Service = require('egg').Service;
const crypto = require('crypto');

class LoginService extends Service {
  /**
   * 管理后台登录
   * @param {object} param 参数
   * @param {string} param.adminname 管理员名
   * @param {string} param.password 管理员名
   * @return {Promise<string>} 返回数据
   */
  async index(param) {
    const { app } = this;
    let { adminname, password } = param;
    const md5 = crypto.createHash('md5');
    password = md5.update(password).digest('hex');
    const checkName = await app.mysql.get('admin', {
      adminname,
      status: 0,
    });
    if (!checkName) {
      throw new Error('登陆失败，用户名不存在');
    }
    const checkPassword = await app.mysql.get('admin', {
      adminname,
      password,
      status: 0,
    });
    if (!checkPassword) {
      throw new Error('登陆失败，密码错误');
    } else {
      return '登陆成功';
    }
  }
}

module.exports = LoginService;
